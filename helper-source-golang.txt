- go mod init gitlab.com/ahkamputra025/learn_golang_api
- go get -u github.com/gin-gonic/gin
- go get github.com/joho/godotenv (as library)
- go get github.com/joho/godotenv/cmd/godotenv (as bin command)
- go get -u gorm.io/gorm
- go get gorm.io/driver/mysql
- go get github.com/dgrijalva/jwt-go
- go get github.com/golang/crypto
- go get github.com/mashingan/smapping

- source gin-> https://github.com/gin-gonic/gin
- source env-> https://github.com/joho/godotenv
- source gorm-> https://gorm.io
- source gorm Mysql-> https://gorm.io/docs/connecting_to_the_database.html
- source code-> https://gitlab.com/pragmaticreviews
- source crypto-> https://github.com/golang/crypto
- source mappingEntiti-> https://github.com/mashingan/smapping

- sudo apt remove golang-go
- https://linuxhint.com/install-go-ubuntu-2/
- https://gorm.io/docs/
- dto-> data transfer object

- $ sudo /opt/lampp/lampp start
- sudo /opt/lampp/manager-linux-x64.run
- mysql -h 127.0.0.1 -P 3306 -u root -p

- source repository-> https://github.com/ydhnwb/golang_heroku