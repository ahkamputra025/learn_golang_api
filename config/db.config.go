package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/ahkamputra025/learn_golang_api/entity"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

//SetupDBConnection is creating a new connection
func SetupDBConnection() *gorm.DB {
	errEnv := godotenv.Load()
	if errEnv != nil {
		panic("Failed to load env file!")
	}

	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")

	dsn := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8&parseTime=True&loc=Local", dbUser, dbPass, dbHost, dbName)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Failed connect to database!")
	}
	//Auto migrate
	db.AutoMigrate(&entity.Book{}, &entity.User{})
	return db
}

//CloseDbConnection method is closing a connection between your app and your db
func CloseDbConnection(db *gorm.DB) {
	dbSQL, err := db.DB()
	if err != nil {
		panic("Failed to close connection from database!")
	}
	dbSQL.Close()
}
