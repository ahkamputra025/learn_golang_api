package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/ahkamputra025/learn_golang_api/dto"
	"gitlab.com/ahkamputra025/learn_golang_api/helper"
	"gitlab.com/ahkamputra025/learn_golang_api/service"
)

//UserController interface is a contract what this controller can do
type UserController interface {
	Update(context *gin.Context)
	Profile(context *gin.Context)
}

type userController struct {
	userService service.UserService
	jwtService  service.JWTService
	authService service.AuthService
}

//NewUserController is creating a new instance of UserController
func NewUserController(userService service.UserService, jwtService service.JWTService, authService service.AuthService) UserController {
	return &userController{
		userService: userService,
		jwtService:  jwtService,
		authService: authService,
	}
}

func (c *userController) Update(context *gin.Context) {
	var userUpdateDTO dto.UserUpdateDTO
	errDTO := context.ShouldBind(&userUpdateDTO)
	if errDTO != nil {
		res := helper.BuildErrorResponse("Failed to process request", errDTO.Error(), helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	authHeader := context.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	id, err := strconv.ParseUint(fmt.Sprintf("%v", claims["user_id"]), 10, 64)
	if err != nil {
		panic(err.Error())
	}

	if !c.authService.IsDuplicateEmail(userUpdateDTO.Email) {
		response := helper.BuildErrorResponse("Failed to process request", "Duplicate email", helper.EmptyObj{})
		context.JSON(http.StatusConflict, response)
	} else {
		userUpdateDTO.ID = id
		u := c.userService.Update(userUpdateDTO)
		res := helper.BuildRespons(true, "OK!", u)
		context.JSON(http.StatusOK, res)
	}
}

func (c *userController) Profile(context *gin.Context) {
	authHeader := context.GetHeader("Authorization")
	token, err := c.jwtService.ValidateToken(authHeader)
	if err != nil {
		panic(err.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	user := c.userService.Profile(fmt.Sprintf("%v", claims["user_id"]))
	res := helper.BuildRespons(true, "OK!", user)
	context.JSON(http.StatusOK, res)
}
